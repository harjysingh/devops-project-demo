# Terraform Google Cloud network Module

This module makes it easy to create VPC, one or more public/private Subnets ,iap-firewall rules, cloud NAT and routers and assign basic permissions on them to arbitrary users.

The resources/services/activations/deletions that this module will create/trigger are:

- One VPC
- One or more Public/Private Subnets
- Zero or more Proxy Subnet
- One or more Cloud Routers and NAT
- Zero or more IAP Firewall

## Compatibility

 This module is meant for use with Terraform 0.12. If you haven't [upgraded](https://www.terraform.io/upgrade-guides/0-12.html)
  and need a Terraform 0.11.x-compatible version of this module, the last released version intended for
  Terraform 0.11.x is [0.1.0](https://registry.terraform.io/modules/terraform-google-modules/cloud-storage/google/0.1.0).

```hcl
module "network" {
  source                      = "./network"
  name_suffix                 = var.name_suffix 
  project                     = var.project
  public_subnet_cidr          = var.public_subnet_cidr
  public_subnet_region        = var.public_subnet_region
  private_subnet_cidr         = var.private_subnet_cidr
  private_subnet_region       = var.private_subnet_region
  router_region               = var.router_region
  proxy_subnet_cidr           = var.proxy_subnet_cidr
  proxy_subnet_region         = var.proxy_subnet_region
  require_iap                 = var.require_iap
}
```

## Providers

| Name | Version |
|------|---------|
| google | n/a |
| google-beta | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| name\_suffix | A name id to be added to GCP resources names. If not provided, project is used as suffix | `string` | n/a | yes |
| project | The ID of the project in which the resource belongs. | `string` | n/a | yes |
| public\_subnet\_cidr | List for the public subnet ip range. | `list(string)` | n/a | yes |
| public\_subnet\_region | List for the public subnet regions. | `list(string)` | n/a | yes |
| private\_subnet\_cidr | List for the private subnet ip range. | `list(string)` | n/a | yes |
| private\_subnet\_region | List for the private subnet regions. | `list(string)` | n/a | yes |
| router\_region | List of region where the routers need to be created | `list(string)` | n/a | yes |
| proxy\_subnet\_cidr | List for the private subnet ip range. | `list(string)` | n/a | yes |
| proxy\_subnet\_region | List for the private subnet regions. | `list(string)` | n/a | yes |
| require_iap | Set to True if IAP Forwarding firewall is required. | `string` | True | yes |

## Outputs

| Name | Description |
|------|-------------|
| firewall | The URI of the created firewall resource. |
| network | The URI of the created VPC resource. |
| private\_subnets | List of the private subnets URI. |
| public\_subnets | List of the public subnets URI. |
| router\_nats | List of IDs of the router nats created with format {{project}}/{{region}}/{{router}}/{{name}}. |
| routers | List of URI of the routers created. |

