locals {
  name_suffix = length(var.name_suffix) > 0 ? var.name_suffix : var.project 
}

data "google_netblock_ip_ranges" "main" {
  range_type = "iap-forwarders"
}


resource "google_compute_network" "main" {
  name                    = "${local.name_suffix}-vpc"
  project                 = var.project
  auto_create_subnetworks = false
  routing_mode            = "GLOBAL"
}

resource "google_compute_subnetwork" "public" {
  count         = length(var.public_subnet_region)
  name          = "${local.name_suffix}-sn-public-${join("", [substr(replace(element(var.public_subnet_region, count.index), "-", ""), 0, 3), substr(element(var.public_subnet_region, count.index), -1, 1)])}-01"
  project       = var.project
  ip_cidr_range = element(var.public_subnet_cidr, count.index)
  region        = element(var.public_subnet_region, count.index)
  network       = google_compute_network.main.self_link

  log_config {
    aggregation_interval = "INTERVAL_5_SEC"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "private" {
  count                    = length(var.private_subnet_region)
  name                     = "${local.name_suffix}-sn-private-${join("", [substr(replace(element(var.private_subnet_region, count.index), "-", ""), 0, 3), substr(element(var.private_subnet_region, count.index), -1, 1)])}-01"
  project                  = var.project
  ip_cidr_range            = element(var.private_subnet_cidr, count.index)
  region                   = element(var.private_subnet_region, count.index)
  network                  = google_compute_network.main.self_link
  private_ip_google_access = var.private_ip_google_access

  log_config {
    aggregation_interval = "INTERVAL_5_SEC"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "proxy" {
  count         = length(var.proxy_subnet_region)
  provider      = google-beta
  name          = "${local.name_suffix}-sn-proxy-${join("", [substr(replace(var.private_subnet_region[1], "-", ""), 0, 3), substr(var.private_subnet_region[1], -1, 1)])}-01"
  project       = var.project
  ip_cidr_range = var.proxy_subnet_cidr[count.index]
  region        = var.proxy_subnet_region[count.index]
  network       = google_compute_network.main.self_link
  purpose       = "INTERNAL_HTTPS_LOAD_BALANCER"
  role          = "ACTIVE"
}

resource "google_compute_firewall" "main" {
  count         = var.require_iap == "True" ?  1 : 0
  name          = "${local.name_suffix}-iap-to-all-fw-ig"
  network       = google_compute_network.main.self_link
  provider      = google-beta
  project       = var.project
  
  log_config {
    metadata    = "INCLUDE_ALL_METADATA"
  }

  allow {
    protocol    = "all"
  }
  source_ranges = concat(data.google_netblock_ip_ranges.main.cidr_blocks_ipv4)
}


resource "google_compute_router" "main" {
  count   = length(var.router_region)
  network = google_compute_network.main.self_link
  project = var.project
  region  = element(var.router_region, count.index)
  name    = "${local.name_suffix}-router-${join("", [substr(replace(element(var.router_region, count.index), "-", ""), 0, 3), substr(element(var.router_region, count.index), -1, 1)])}-01"
}

resource "google_compute_router_nat" "main" {
  depends_on                         = [google_compute_router.main]
  count                              = length(var.router_region)
  project                            = var.project
  name                               = "${local.name_suffix}-nat-${join("", [substr(replace(element(var.router_region, count.index), "-", ""), 0, 3), substr(element(var.router_region, count.index), -1, 1)])}-01"
  region                             = element(var.router_region, count.index)
  router                             = "${local.name_suffix}-router-${join("", [substr(replace(element(var.router_region, count.index), "-", ""), 0, 3), substr(element(var.router_region, count.index), -1, 1)])}-01"
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_PRIMARY_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}
