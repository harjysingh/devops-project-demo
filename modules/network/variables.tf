variable "project" {
  description = "The ID of the project in which the resource belongs."
}

variable "require_iap" {
  description = "Set to True if IAP Forwarding firewall is required"
  default     = "True"
}
variable "public_subnet_region" {
  description = "List for the public subnet regions."
  type        = list(string)
}

variable "public_subnet_cidr" {
  description = "List for the public subnet ip range."
  type        = list(string)
}

variable "private_subnet_region" {
  description = "List for the private subnet regions."
  type        = list(string)
}

variable "private_subnet_cidr" {
  description = "List for the private subnet ip range."
  type        = list(string)
}

variable "router_region" {
  description = "List of region where the routers need to be created"
}
variable "proxy_subnet_cidr" {
  description = "Subnet range to be used by Proxy subnet"
  type        = list(string)
  default     = []
}
variable "proxy_subnet_region" {
  description = "List for the proxy subnet regions."
  type        = list(string)
  default     = []
}
variable "private_ip_google_access" {
  description = "When enabled, VMs in this subnetwork without external IP addresses can access Google APIs and services by using Private Google Access."
  default     = true
}
variable "name_suffix" {
  description = "A name id to be added to GCP resources names"
}