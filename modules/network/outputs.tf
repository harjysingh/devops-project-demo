output "network" {
  value       = google_compute_network.main.self_link
  description = "The URI of the created VPC resource."
}

output "id" {
  value = google_compute_network.main.id
  description = "an identifier for the resource with format projects/{{project}}/global/networks/{{name}}"
}

output "private_subnets" {
  value       = google_compute_subnetwork.private[*].self_link
  description = "List of the private subnets URI."
}

output "public_subnets" {
  value       = google_compute_subnetwork.public[*].self_link
  description = "List of the public subnets URI."
}

output "routers" {
  value       = google_compute_router.main[*].self_link
  description = "List of URI of the routers created."
}

output "router_nats" {
  value       = google_compute_router_nat.main[*].id
  description = "List of IDs of the router nats created with format {{project}}/{{region}}/{{router}}/{{name}}."
}

output "firewall" {
  value       = google_compute_firewall.main[*].self_link
  description = "The URI of the created firewall resource."
}
