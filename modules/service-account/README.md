# Terraform Google Cloud Service Account Module
Copyright 2019 Google LLC

Licensed under the Apache License, Version 2.0 (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.

This module makes it easy to create a secret, and assign basic permissions on them to arbitrary service accounts and groups.

The resources/services/activations/deletions that this module will create/trigger are:

- One Secret
- Version for the created secret
- Zero or more IAM bindings for those buckets

## Compatibility

 This module is meant for use with Terraform 0.12. If you haven't [upgraded](https://www.terraform.io/upgrade-guides/0-12.html)
  and need a Terraform 0.11.x-compatible version of this module, the last released version intended for
  Terraform 0.11.x is [0.1.0](https://registry.terraform.io/modules/terraform-google-modules/cloud-storage/google/0.1.0).

## Usage
```hcl
module "service-account" {
  source = "git::https://bitbucket.org/hollywoodpark/hwp-plt-terraform.git//service-account"
  project     = var.project
  wstream     = var.wstream
  env         = var.env
  suffix = ["jenkins"]
  project_roles = [
    "hwp-development=>roles/storage.admin",
  ]
}
```

## Providers

| Name | Version |
|------|---------|
| google | n/a |
| template | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| billing\_account\_id | If assigning billing role, specificy a billing account (default is to assign at the organizational level). | `string` | n/a | yes |
| generate\_keys | Generate keys for service accounts. | `bool` | n/a | yes |
| grant\_billing\_role | Grant billing user role. | `bool` | n/a | yes |
| grant\_xpn\_roles | Grant roles for shared VPC management. | `bool` | n/a | yes |
| names | Names of the service accounts to create. | `list(string)` | n/a | yes |
| org\_id | Id of the organization for org-level roles. | `string` | n/a | yes |
| prefix | Prefix applied to service account names. | `string` | n/a | yes |
| project\_id | Project id where service account will be created. | `string` | n/a | yes |
| project\_roles | Common roles to apply to all service accounts, project=>role as elements. | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| email | Service account email (for single use). |
| emails | Service account emails by name. |
| emails\_list | Service account emails as list. |
| iam\_email | IAM-format service account email (for single use). |
| iam\_emails | IAM-format service account emails by name. |
| iam\_emails\_list | IAM-format service account emails as list. |
| key | Service account key (for single use). |
| keys | Map of service account keys. |
| service\_account | Service account resource (for single use). |
| service\_accounts | Service account resources as list. |
| service\_accounts\_map | Service account resources by name. |

