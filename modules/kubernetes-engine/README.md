# Terraform Google Cloud Kubernetes Module

This module makes it easy to project Metadata items.

The resources/services/activations/deletions that this module will create/trigger are:
- One GKE Cluster
- One GKE Node Pool

## Providers

| Name | Version |
|------|---------|
| google | >= 2.18, <4.0.0 |
| kubernetes | n/a |
| null | n/a |
| random | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| basic\_auth\_password | The password to be used with Basic Authentication. | `string` | n/a | yes |
| basic\_auth\_username | The username to be used with Basic Authentication. An empty value will disable Basic Authentication, which is the recommended configuration. | `string` | n/a | yes |
| cluster\_ipv4\_cidr | The IP address range of the kubernetes pods in this cluster. Default is an automatically assigned CIDR. | `any` | n/a | yes |
| cluster\_resource\_labels | The GCE resource labels (a map of key/value pairs) to be applied to the cluster | `map(string)` | n/a | yes |
| configure\_ip\_masq | Enables the installation of ip masquerading, which is usually no longer required when using aliasied IP addresses. IP masquerading uses a kubectl call, so when you have a private cluster, you will need access to the API server. | `any` | n/a | yes |
| create\_service\_account | Defines if service account specified to run nodes should be created. | `bool` | n/a | yes |
| deploy\_using\_private\_endpoint | (Beta) A toggle for Terraform and kubectl to connect to the master's internal IP address during deployment. | `bool` | n/a | yes |
| description | The description of the cluster | `string` | n/a | yes |
| disable\_legacy\_metadata\_endpoints | Disable the /0.1/ and /v1beta1/ metadata server endpoints on the node. Changing this value will cause all node pools to be recreated. | `bool` | n/a | yes |
| enable\_private\_endpoint | (Beta) Whether the master's internal IP address is used as the cluster endpoint | `bool` | n/a | yes |
| enable\_private\_nodes | (Beta) Whether nodes have internal IP addresses only | `bool` | n/a | yes |
| grant\_registry\_access | Grants created cluster-specific service account storage.objectViewer role. | `bool` | n/a | yes |
| horizontal\_pod\_autoscaling | Enable horizontal pod autoscaling addon | `bool` | n/a | yes |
| http\_load\_balancing | Enable httpload balancer addon | `bool` | n/a | yes |
| initial\_node\_count | The number of nodes to create in this cluster's // default node pool. | `number` | n/a | yes |
| ip\_masq\_link\_local | Whether to masquerade traffic to the link-local prefix (169.254.0.0/16). | `bool` | n/a | yes |
| ip\_masq\_resync\_interval | The interval at which the agent attempts to sync its ConfigMap file from the disk. | `string` | n/a | yes |
| ip\_range\_pods | The _name_ of the secondary subnet ip range to use for pods | `string` | n/a | yes |
| ip\_range\_services | The _name_ of the secondary subnet range to use for services | `string` | n/a | yes |
| issue\_client\_certificate | Issues a client certificate to authenticate to the cluster endpoint. To maximize the security of your cluster, leave this option disabled. Client certificates don't automatically rotate and aren't easily revocable. WARNING: changing this after cluster creation is destructive! | `bool` | n/a | yes |
| kubernetes\_version | The Kubernetes version of the masters. If set to 'latest' it will pull latest available version in the selected region. | `string` | n/a | yes |
| logging\_service | The logging service that the cluster should write logs to. Available options include logging.googleapis.com, logging.googleapis.com/kubernetes (beta), and none | `string` | n/a | yes |
| maintenance\_start\_time | Time window specified for daily or recurring maintenance operations in RFC3339 format | `string` | n/a | yes |
| master\_authorized\_networks | List of master authorized networks. If none are provided, disallow external access (except the cluster node IPs, which GKE automatically whitelists). | `list(object({ cidr_block = string, display_name = string }))` | n/a | yes |
| master\_ipv4\_cidr\_block | (Beta) The IP range in CIDR notation to use for the hosted master network | `string` | n/a | yes |
| monitoring\_service | The monitoring service that the cluster should write metrics to. Automatically send metrics from pods in the cluster to the Google Cloud Monitoring API. VM metrics will be collected by Google Compute Engine regardless of this setting Available options include monitoring.googleapis.com, monitoring.googleapis.com/kubernetes (beta) and none | `string` | n/a | yes |
| name | The name of the cluster (required) | `string` | n/a | yes |
| network | The VPC network to host the cluster in (required) | `string` | n/a | yes |
| network\_policy | Enable network policy addon | `bool` | n/a | yes |
| network\_policy\_provider | The network policy provider. | `string` | n/a | yes |
| network\_project\_id | The project ID of the shared VPC's host (for shared vpc support) | `string` | n/a | yes |
| node\_pools | List of maps containing node pools | `list(map(string))` | n/a | yes |
| node\_pools\_labels | Map of maps containing node labels by node-pool name | `map(map(string))` | n/a | yes |
| node\_pools\_metadata | Map of maps containing node metadata by node-pool name | `map(map(string))` | n/a | yes |
| node\_pools\_oauth\_scopes | Map of lists containing node oauth scopes by node-pool name | `map(list(string))` | n/a | yes |
| node\_pools\_tags | Map of lists containing node network tags by node-pool name | `map(list(string))` | n/a | yes |
| node\_version | The Kubernetes version of the node pools. Defaults kubernetes\_version (master) variable and can be overridden for individual node pools by setting the `version` key on them. Must be empyty or set the same as master at cluster creation. | `string` | n/a | yes |
| non\_masquerade\_cidrs | List of strings in CIDR notation that specify the IP address ranges that do not use IP masquerading. | `list(string)` | n/a | yes |
| project\_id | The project ID to host the cluster in (required) | `string` | n/a | yes |
| region | The region to host the cluster in (optional if zonal cluster / required if regional) | `string` | n/a | yes |
| regional | Whether is a regional cluster (zonal cluster if set false. WARNING: changing this after cluster creation is destructive!) | `bool` | n/a | yes |
| registry\_project\_id | Project holding the Google Container Registry. If empty, we use the cluster project. If grant\_registry\_access is true, storage.objectViewer role is assigned on this project. | `string` | n/a | yes |
| remove\_default\_node\_pool | Remove // default node pool while setting up the cluster | `bool` | n/a | yes |
| service\_account | The service account to run nodes as if not overridden in `node_pools`. The create\_service\_account variable // default value (true) will cause a cluster-specific service account to be created. | `string` | n/a | yes |
| skip\_provisioners | Flag to skip all local-exec provisioners. It breaks `stub_domains` and `upstream_nameservers` variables functionality. | `bool` | n/a | yes |
| stub\_domains | Map of stub domains and their resolvers to forward DNS queries for a certain domain to an external DNS server | `map(list(string))` | n/a | yes |
| subnetwork | The subnetwork to host the cluster in (required) | `string` | n/a | yes |
| upstream\_nameservers | If specified, the values replace the nameservers taken by // default from the node’s /etc/resolv.conf | `list(string)` | n/a | yes |
| zones | The zones to host the cluster in (optional if regional cluster / required if zonal) | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| ca\_certificate | Cluster ca certificate (base64 encoded) |
| endpoint | Cluster endpoint |
| horizontal\_pod\_autoscaling\_enabled | Whether horizontal pod autoscaling enabled |
| http\_load\_balancing\_enabled | Whether http load balancing enabled |
| location | Cluster location (region if regional cluster, zone if zonal cluster) |
| logging\_service | Logging service used |
| master\_authorized\_networks\_config | Networks from which access to master is permitted |
| master\_version | Current master kubernetes version |
| min\_master\_version | Minimum master kubernetes version |
| monitoring\_service | Monitoring service used |
| name | Cluster name |
| network\_policy\_enabled | Whether network policy enabled |
| node\_pools\_names | List of node pools names |
| node\_pools\_versions | List of node pools versions |
| region | Cluster region |
| service\_account | The service account to default running nodes as if not overridden in `node_pools`. |
| type | Cluster type (regional / zonal) |
| zones | List of zones in which the cluster resides |

