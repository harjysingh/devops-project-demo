from flask import Flask, render_template
import config
from loguru import logger
from healthcheck import HealthCheck, EnvironmentDump

app = Flask(__name__)
app.debug = True


health = HealthCheck(app, "/hc")
envdump = EnvironmentDump(app, "/env",
                        include_python=False, include_os=False, 
                        include_process=False, include_config=False)

def health_check():
    return True, "Flask App is running; Healthcheck Passed"

health.add_check(health_check)

def application_data():
	return {"maintainer": "Harjyot Singh",
            "application" : "FLASK-DEVOPS-DEMO-APP"}

envdump.add_section("application", application_data)

@app.route('/')
def home():
    return render_template("home.html")

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=config.PORT, debug=config.DEBUG_MODE)