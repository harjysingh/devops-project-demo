provider "google" {
    region  = var.region
}

module "network" {
    source                      = "./modules/network"
    name_suffix                 = "tf-testing" 
    project                     = var.project
    public_subnet_cidr          = ["10.0.0.0/22","10.0.4.0/22"]
    public_subnet_region        = ["us-east1","us-east4"]
    private_subnet_cidr         = ["10.0.8.0/22","10.0.12.0/22","10.0.104.0/22"]
    private_subnet_region       = ["us-east1","us-east4","us-central1" ]
    # proxy_subnet_cidr           = ["10.0.144.0/24"]
    # proxy_subnet_region         = ["us-east4"]
    proxy_subnet_cidr           = []
    proxy_subnet_region         = []
    router_region               = ["us-east1","us-east4","us-central1"]
    require_iap                 = "False"
}

module "k8-sa" {
    depends_on    = [module.network]
    source        = "./modules/service-account"
    name_suffix   = "tf-testing" 
    project       = var.project
    generate_keys = false
    suffix        = ["k8"]
    project_roles = []
                        # "${var.project}=>roles/secretmanager.secretAccessor",
                        # "${var.project}=>roles/secretmanager.viewer",
                        # "${var.project}=>roles/secretmanager.admin",
                        # "${var.project}=>roles/iam.workloadIdentityUser",
                    # ]
}

data "google_compute_subnetwork" "subnetwork" {
    depends_on = [module.network]
    project    = var.project
    name       = "tf-testing-sn-private-usc-01"
    region     = "us-central1"
}


module "kubernetes-engine" {
    # depends_on = [module.k8-sa]
    source = "./modules/kubernetes-engine"
    description = "GKE clsuter created via Terraform"
    initial_node_count = "1"
    kubernetes_version = "latest"
    name = var.cluster_name
    network = "tf-testing-vpc"
    project_id = var.project
    region = var.region
    subnetwork = var.subnet_name
    master_ipv4_cidr_block = "172.16.112.0/28"
    ip_range_pods = "/20"
    ip_range_services = "/20"
    create_service_account = false
    maintenance_start_time = "16:30"
    service_account = module.k8-sa.email
    node_pools = [
        {
        name               = "hs-default-node-pool"
        machine_type       = "n1-standard-2"
        min_count          = 1
        max_count          = 10
        disk_size_gb       = 20
        },
    ]
    master_authorized_networks = [
        # {
        # cidr_block   = "167.219.0.0/16",
        # display_name = "Deloitte"
        # },
        # {
        # cidr_block   = "103.76.232.0/22",
        # display_name = "Deloitte Hyderabad"
        # },
    ]       
}