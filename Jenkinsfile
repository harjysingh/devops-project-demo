#!groovy
properties([
    parameters([
        choice(name: 'command', choices: ['CREATE', 'DESTROY', 'SKIP'], description: 'Select CREATE or DESTROY depending on your requirement.'),
        string(name: 'project_name', description: 'Enter the name for GCP Project.',defaultValue: 'playground-s-11-8757cbc0'),
        string(name: 'region', description: 'Enter the region for GCP resources.',defaultValue: 'us-central1'),
        string(name: 'zone', description: 'Enter the region for GCP resources.',defaultValue: 'us-central1-f'),
        string(name: 'subnet_name', description: 'Enter the subnet name for GCP resources.',defaultValue: 'tf-testing-sn-private-usc1-01'),
        string(name: 'tag', description: 'Enter the tag for docker Image.',defaultValue: '1.0'),
        string(name: 'cluster_name', description: 'Enter thename for Kubernetes cluster.',defaultValue: 'tf-testing-k8-cluster'),
        ])
    ])


    def _project_id     = params.project_name
    def _region         = params.region
    def _zone           = params.zone
    def _subnet_name    = params.subnet_name
    def _tag            = params.tag
    def _cluster_name   = params.cluster_name

    _tfvars = "project = \"${params.project_name}\"\r\nregion = \"${params.region}\"\r\nzone = \"${params.zone}\"\r\nsubnet_name = \"${params.subnet_name}\"\r\ncluster_name = \"${params.cluster_name}\""

pipeline {
    options {
      ansiColor('xterm')
    }
    agent any
    // agent {
    //     dockerfile true
    // }

    environment {
        BITBUCKET_CREDENTIALS_ID='bitbucket'        //the bitbucket credential ID in Jenkins
        BITBUCKET_URL = 'https://harjysingh@bitbucket.org/harjysingh/devops-project-demo.git'
        TF_STATE_BUCKET='devops-bootcamp-gcs-tf-state'
        WORKING_DIR = 'main'
        FEATURE_BRANCH_NAME = "main"
        PROJECT_NAME= "${params.project_name}"
        TERRAFORM_COMMAND= "${params.command}"
        REGION="${_region}"
        ZONE="${_zone}"
        SUBNET_NAME="${_subnet_name}"
        TAG="${params.tag}"
        CLUSTER_NAME="${_cluster_name}"
        TF_VARS_TEXT = "${_tfvars}"
        }

    stages {
        stage('CheckOut: Bitbucket Repo') {
            steps {
            // container(CONTAINER_NAME) {
            dir(WORKING_DIR) {
            checkout (
                changelog: false,
                poll: false,
                scm: [
                $class: 'GitSCM', branches: [
                    [name: "${FEATURE_BRANCH_NAME}"]
                ],
                doGenerateSubmoduleConfigurations: false,
                extensions: [],
                submoduleCfg: [],
                userRemoteConfigs: [
                    [credentialsId: "${BITBUCKET_CREDENTIALS_ID}", url: "${BITBUCKET_URL}"]
                    ]
                    ]
                )
                }
            // }
        }
        }

        stage('CHECKOUT: Bitbucket Credentials') {
            steps {
                // container(CONTAINER_NAME) {
                dir(WORKING_DIR) {
                withCredentials([usernamePassword(credentialsId: BITBUCKET_CREDENTIALS_ID, passwordVariable: 'GIT_PASS', usernameVariable: 'GIT_USER')]) {
                    sh'''
                    set +x
                    export GIT_PASSWORD="$(echo $GIT_PASS | sed 's/@/%40/g')"
                    echo https://$GIT_USER:$GIT_PASSWORD@bitbucket.org >> ./.git-credentials
                    mv ./.git-credentials ~/.git-credentials
                    git config --global credential.helper 'store --file ~/.git-credentials'
                    set -x
                    '''
                }
                }
            // }
            }
        }
        // stage('Sonarqube') {
        //     environment {
        //         scannerHome = tool 'SonarQubeScanner'
        //     }
        //     steps {
        //         withSonarQubeEnv('sonarqube') {
        //             sh "${scannerHome}/bin/sonar-scanner"
        //         }
        //         timeout(time: 10, unit: 'MINUTES') {
        //             waitForQualityGate abortPipeline: true
        //         }
        //     }
        // }
        stage('Authenticating Google SDK') {
            steps {
                withCredentials([file(credentialsId: 'gcp-servicecredential', variable: 'GCP_SERVICE_CRED')])
                 {
                    sh 'sudo cp $GCP_SERVICE_CRED ./serviceaccount.json'
                    sh 'sudo setfacl -m u:jenkins:rwx  ./serviceaccount.json'
                    }
                sh 'pwd'
                sh 'gcloud auth activate-service-account --key-file=."/serviceaccount.json"'
                sh 'export GOOGLE_APPLICATION_CREDENTIALS="./serviceaccount.json"'
                sh 'gcloud auth list'
            }
        }

        stage ('Update User Variables') {
            steps {
                dir(WORKING_DIR) {
                writeFile  file: 'main.tfvars', text: "${TF_VARS_TEXT}"
                echo "TERRAFORM VARIABLES:"
                sh 'cat main.tfvars'
                sh '''
                PATH=/usr/local/bin
                terraform --version
                '''
            }
            // }
            }
        }
        

    stage('INITIALIZE: Terraform Backend') {
        steps {
            script{
            if (TERRAFORM_COMMAND == 'SKIP') {
                echo 'Skipping this Stage'
            } else {
              sh('')
              writeFile  file: 'backend.tf', text: '''
              terraform {
                  backend "gcs"{
                }
                }
              '''
              sh '''
              export GOOGLE_APPLICATION_CREDENTIALS="./serviceaccount.json"
              PATH=/usr/local/bin
              terraform init \
                -backend-config="bucket=\"devops-bootcamp-gcs-tf-state-6\"" \
                -backend-config="credentials=\"./serviceaccount.json\"" \
                -backend-config="prefix=\"DEVOPS_BOOTCAMP_DEMO\"" \
                -input=false
            '''
                }
            }
        }
      }

      stage('GENERATE: Terraform Plan') {
        steps {
            script {
            if (TERRAFORM_COMMAND == 'CREATE') {
                sh '''
                export GOOGLE_APPLICATION_CREDENTIALS="./serviceaccount.json"
                PATH=/usr/local/bin
                terraform plan -var-file=./main/main.tfvars -out tfplan -input=false
                '''
            } else if (TERRAFORM_COMMAND == 'DESTROY') {
                sh '''
                export GOOGLE_APPLICATION_CREDENTIALS="./serviceaccount.json"
                PATH=/usr/local/bin
                terraform plan -var-file=./main/main.tfvars -out tfplan -input=false -destroy'''
            } else if (TERRAFORM_COMMAND == 'SKIP') {
                    echo 'Skipping this Stage'
            } else {
                echo 'Invalid Terraform Execution Command. Can be one of either CREATE/DESTROY/SKIP !!!'
            }
        }
        }
      }

      stage('APPROVE: Terraform Plan') {
        steps {
          script {
            if (TERRAFORM_COMMAND == 'SKIP') {
                echo 'Skipping this Stage'
            } else {
                def userInput = input(message: 'Apply Terraform?')
            }
          }
        }
      }

    stage('APPLY: Terraform Plan') {
      steps {
        script {
            if (TERRAFORM_COMMAND == 'CREATE') {
            sh '''
            export GOOGLE_APPLICATION_CREDENTIALS="./serviceaccount.json"
            PATH=/usr/local/bin
            terraform apply -input=false tfplan'''
            } else if (TERRAFORM_COMMAND == 'DESTROY') {
            sh '''
            export GOOGLE_APPLICATION_CREDENTIALS="./serviceaccount.json"
            PATH=/usr/local/bin
            terraform destroy -var-file=./main/main.tfvars -auto-approve'''
            } else if (TERRAFORM_COMMAND == 'SKIP') {
                echo 'Skipping this Stage'
            } else {
            echo 'Invalid Terraform Execution Command. Can be one of either CREATE/DESTROY/SKIP !!!'
            }              
        }
        }
      }
    stage('Update Image Tag') {
        steps {
            sh 'echo "Skipping"'
            // sh'''
            // gcloud container images untag gcr.io/$PROJECT_NAME/harjys/${SERVICE_NAME}-app:latest --quiet
            // '''
        }
        }
    stage('Build Image') {
        steps {
        script { 
            if (TERRAFORM_COMMAND == 'DESTROY') {
                echo 'Skipping this stage!!'
             } else {
                sh """
                export GOOGLE_APPLICATION_CREDENTIALS="./serviceaccount.json"
                gcloud builds submit --tag gcr.io/$PROJECT_NAME/ml_model/flask-app:v$TAG ./docker-project/ --project=$PROJECT_NAME
                gcloud container images add-tag gcr.io/$PROJECT_NAME/ml_model/flask-app:v$TAG gcr.io/$PROJECT_NAME/ml_model/flask-app:latest --quiet
            """
                }
            }
        }
    }
    stage('Update K8s Deployment') {
        steps {
        script {
            if (TERRAFORM_COMMAND == 'CREATE') {
            sh """
            export GOOGLE_APPLICATION_CREDENTIALS="../serviceaccount.json"
            gcloud container clusters get-credentials $CLUSTER_NAME --region $REGION --project $PROJECT_NAME
            kubectl apply -f ./deployment.yaml
            """
            } else if (TERRAFORM_COMMAND == 'DESTROY') {
            echo 'Skipping this stage!!'
            } else if (TERRAFORM_COMMAND == 'SKIP') {
            sh """
            gcloud container clusters get-credentials $CLUSTER_NAME --region $REGION --project $PROJECT_NAME
            kubectl set image deployment/flask-app-deployment flask-app=gcr.io/$PROJECT_NAME/ml_model/flask-app:v$TAG --record
            """
            } else {
            echo 'Invalid Terraform Execution Command. Can be one of either CREATE/DESTROY/SKIP !!!'
            }              
        }

            sh 'rm ./serviceaccount.json'
            }
        }
        }
    }